//
//  CKRecordZone+Rx.swift
//  iCloudStore
//
//  Created by innovapost on 2017-09-30.
//  Copyright © 2017 Swifter. All rights reserved.
//

/* Notes from CKRecordZone API documentation:
 1. You cannot save any records in the zone until you save it to the database.
 2. When creating records, explicitly specify the zone ID if you want the records to reside in a specific zone; otherwise, they will be saved to the default zone.
 3. You cannot create custom zones in a public database.
 */
/* My Notes:
 * 1. For Default Zone: You cant fetch incremental changes since any time.
 */
import CloudKit
import RxSwift

extension Reactive where Base == CKRecordZone {
  public func create(usingOperation operation:CKModifyRecordZonesOperation = CKModifyRecordZonesOperation()) -> Observable<CKRecordZoneID>{
    return Observable.create({ (observer) -> Disposable in
      operation.recordZonesToSave = [self.base]
      operation.modifyRecordZonesCompletionBlock = { recordZones, _ , error in
        if let error = error { observer.onError(error) }
        else if let recordZones = recordZones { observer.onNext(recordZones.first!.zoneID) }
        observer.onCompleted()
      }
      CKContainer.default().privateCloudDatabase.add(operation)
      return Disposables.create()
    })
  }
  
  public func fetchCapabilities (usingOperation operation:CKFetchRecordZonesOperation = CKFetchRecordZonesOperation())->Observable<CKRecordZoneCapabilities>{
    return Observable.create({ (observer) -> Disposable in
      operation.recordZoneIDs = [self.base.zoneID]
      operation.fetchRecordZonesCompletionBlock = { zones , error in
        if let error = error { observer.onError(error) }
        else if let zones = zones { observer.onNext(zones.first!.value.capabilities) }
        observer.onCompleted()
      }
      CKContainer.default().privateCloudDatabase.add(operation)
      return Disposables.create()
    })
  }
  
  
  public func delete(usingOperation operation:CKModifyRecordZonesOperation = CKModifyRecordZonesOperation()) -> Observable<CKRecordZoneID>{
    return Observable.create({ (observer) -> Disposable in
      operation.recordZoneIDsToDelete = [self.base.zoneID]
      operation.modifyRecordZonesCompletionBlock = { _, recordsZonesIDs , error in
        if let error = error { observer.onError(error) }
        else if let recordsZonesIDs = recordsZonesIDs { observer.onNext(recordsZonesIDs.first!) }
        observer.onCompleted()
      }
      CKContainer.default().privateCloudDatabase.add(operation)
      return Disposables.create()
    })
  }
}
