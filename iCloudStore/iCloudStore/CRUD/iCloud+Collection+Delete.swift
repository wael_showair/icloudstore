//
//  iCloud+Collection+Delete.swift
//  iCloudStore
//
//  Created by innovapost on 2017-09-22.
//  Copyright © 2017 Swifter. All rights reserved.
//

import CloudKit
import RxSwift

extension Reactive where Base == ICloudStore{
  public func delete (_ deletables:[ICloudStorable],usingOperationConfiguration configuration:CKOperationConfiguration?=nil)->Observable<[CKRecordID]>{
    
    return self.push(storables: nil, andDelete: deletables, usingOperationConfiguration: configuration)
      .map({ (result) -> [CKRecordID]? in
        switch result{
        case .allRecordsIDsResultType(let recordsIDs):
          return recordsIDs
        default:
          return nil
        }
      })
      .unwrap()
  }
}
