//
//  iCloudStore.h
//  iCloudStore
//
//  Created by innovapost on 2017-09-11.
//  Copyright © 2017 Swifter. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for iCloudStore.
FOUNDATION_EXPORT double iCloudStoreVersionNumber;

//! Project version string for iCloudStore.
FOUNDATION_EXPORT const unsigned char iCloudStoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <iCloudStore/PublicHeader.h>

#import <iCloudStore/Reachability.h>
