//
//  AppDelegate.swift
//  ExprimentalApp
//
//  Created by innovapost on 2017-09-11.
//  Copyright © 2017 Swifter. All rights reserved.
//

import UIKit
import CloudKit
import iCloudStore
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  let iCloud = ICloudStore()
  let networkSurveyor = NetworkSurveyor.shared
  let bag = DisposeBag()
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    
    //MARK:- Create zone and relative subscription
    let iCloud = ICloudStore()

    //MARK:- DI iCloud store into the root view controller
    let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    if let viewController = storyboard.instantiateInitialViewController() as? ViewController {
      viewController.iCloud = iCloud
      viewController.networkSurveyor = NetworkSurveyor.shared
      window?.rootViewController = viewController
    }
    
    return true
  }
}

