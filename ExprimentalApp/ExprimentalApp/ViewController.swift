//
//  ViewController.swift
//  ExprimentalApp
//
//  Created by innovapost on 2017-09-11.
//  Copyright © 2017 Swifter. All rights reserved.
//

import UIKit
import CloudKit
import iCloudStore
import RxSwift

class ViewController: UIViewController {
  var iCloud: ICloudStore!
  var networkSurveyor: NetworkSurveyor!
  let bag = DisposeBag()
  let persons = [
    Person(name: "name-1", age: 13, uuid: UUID().uuidString),
    Person(name: "name-2", age: 34, uuid: UUID().uuidString),
    Person(name: "name-3", age: 25, uuid: UUID().uuidString),
    Person(name: "name-4", age: 10, uuid: UUID().uuidString),
    ]
  override func viewDidLoad() {
    super.viewDidLoad()
    networkSurveyor.start()
    networkSurveyor.status.asObservable().subscribe(onNext:{ status in
      DispatchQueue.main.async {
        self.networkStatus.text = "\(status)"
      }
    })
      .disposed(by: bag)
    
    iCloud.account.status.asObservable().subscribe(onNext:{ status in
      DispatchQueue.main.async {
        self.loginStatus.text = "\(self.iCloud.account.status.value)"
      }
    })
      .disposed(by: bag)
  }
  
  @IBOutlet weak var networkStatus: UILabel!
  @IBOutlet weak var operationStatus: UILabel!
  @IBOutlet weak var loginStatus: UILabel!
  @IBAction func onPushingToICloud(_ sender: UIButton) {
    self.push()
  }
    
  func executeHandlers(for error: Error){
    let iCloudError = self.create(from: error)
    iCloudError.conveyToUI()
  }
  
  func push(usingConfiguration configuration:CKOperationConfiguration?=nil) {
    iCloud.rx.push(persons, usingOperationConfiguration: configuration)
      .do(onError:{ self.executeHandlers(for: $0)})
      .recoverFromError()
      .subscribe(onCompleted: {
        DispatchQueue.main.async {
          self.operationStatus.text = "Completed"
          self.operationStatus.textColor = UIColor.green
        }
      })
      .disposed(by: bag)
  }
  
  func onZoneNotFound(){
    DispatchQueue.main.async {
      self.operationStatus.text = "Zone Not Found"
      self.operationStatus.textColor = UIColor.red
    }
  }
}

extension ViewController {
  func create(from error:Error) -> CKError {
    var cloudKitError = CKError.init(_nsError: error as NSError)
    switch cloudKitError.code {
    case .notAuthenticated:
      cloudKitError.handler = onNonAuthenticated
    case .networkUnavailable:
      cloudKitError.handler = onNetworkUnavailable
    case .networkFailure:
      cloudKitError.handler = onNetworkFailure
    case .zoneNotFound:
      cloudKitError.handler = onZoneNotFound
    default:
      print("****************** ViewController:unexpected error\n \(error as? CKError as Any)")
    }
    return cloudKitError
  }
}
