//
//  Person.swift
//  ExprimentalApp
//
//  Created by innovapost on 2017-11-04.
//  Copyright © 2017 Swifter. All rights reserved.
//

import Foundation
import iCloudStore
import CloudKit

struct Person{
  let name:String, age: Int16, uuid:String
}
extension Person {
  static var iCloudRecordType:String{ return "Person" }
  static var iCloudZoneName:String { return ICloudStore.zone.zoneID.zoneName }
}

extension Person: Equatable{
  static func == (lhs: Person, rhs: Person) -> Bool {
    return lhs.uuid == rhs.uuid
  }
}
extension Person:ICloudStorable{
  func toCloudObject() -> CKRecord {
    let zoneID = ICloudStore.zone.zoneID //CKRecordZoneID(zoneName: Person.iCloudZoneName , ownerName: CKCurrentUserDefaultName )
    let id = CKRecordID(recordName: self.uuid, zoneID: zoneID)
    let record = CKRecord(recordType: Person.iCloudRecordType, recordID: id)
    record.setValue(self.name, forKey: "name")
    record.setValue(self.age, forKey: "age")
    return record
  }
}

extension CKRecord{
  func toDataObject()->Person {
    return Person(name: self.value(forKey: "name") as! String,
                  age: self.value(forKey: "age") as! Int16,
                  uuid: self.recordID.recordName)
    
  }
}
