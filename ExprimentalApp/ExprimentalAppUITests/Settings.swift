//
//  Settings.swift
//  ExprimentalAppUITests
//
//  Created by innovapost on 2017-11-05.
//  Copyright © 2017 Swifter. All rights reserved.
//

import XCTest
struct Settings {
  static var app:XCUIApplication = XCUIApplication(bundleIdentifier: "com.apple.Preferences")
}

extension Settings {
  struct iCloudDrive {
    static let targetAppName = "ExprimentalApp"
    
    static func turnOff() {
      app.activate()
      app.tables.cells.element(boundBy: 0).tap()
      app.tables.cells.staticTexts["iCloud"].tap()
      app.tables.switches[targetAppName].tap()
    }
    
    static func turnOn() {
      app.activate()
      app.tables.switches[targetAppName].tap()
    }
  }
}

extension Settings {
  struct Airplane {
    static let text = "Airplane Mode"
    static func turnOn() {
      app.activate()
      WiFi.turnOff()
      app.tables.switches[text].tap()
    }
    
    static func turnOff() {
      app.activate()
      WiFi.turnOn()
      app.tables.switches[text].tap()
    }
  }
}

extension Settings{
  struct WiFi {
    static let wifiCell = app.tables.cells["Wi-Fi"]
    static let wifiSwitch = app.tables.switches["Wi-Fi"]
    static let backButton = app.navigationBars["Wi-Fi"].buttons["Settings"]
    static func turnOn(){
      if("Off" == wifiCell.value as! String){
        wifiCell.tap()
        wifiSwitch.tap()
        backButton.tap()
      }
    }
    static func turnOff(){
      if !(wifiCell.staticTexts["Not Connected"].exists || wifiCell.staticTexts["Off"].exists){
        wifiCell.tap()
        wifiSwitch.tap()
        backButton.tap()
      }
    }
  }
}
