//
//  ExprimentalAppUITests.swift
//  ExprimentalAppUITests
//
//  Created by innovapost on 2017-10-01.
//  Copyright © 2017 Swifter. All rights reserved.
//

import XCTest
class AuthenticationErrorUITests: XCTestCase {
  
  let app = XCUIApplication()
  
  override func setUp() {
    super.setUp()
    // In UI tests it is usually best to stop immediately when a failure occurs.
    continueAfterFailure = false
    app.launch()
    Settings.app.launch()
    XCTContext.runActivity(named: "Turn Off iCloud Drive in Settings App") { _ in
      Settings.iCloudDrive.turnOff()
    }
    app.activate()
  }
  
  override func tearDown() {
    XCTContext.runActivity(named: "Turn On iCloud Drive in Settings App") { _ in
      Settings.iCloudDrive.turnOn()
    }
    Settings.app.terminate()
    super.tearDown()
  }
  
  func testNotAuthenticatedError() {
    
    app.buttons["Auth Fail"].tap()
    
    //TODO:- Use localized accessible text instead of hard coded string.
    let errorLabel = app.staticTexts["Not Authenticated"]
    XCTestAssertExistence(of: errorLabel)
  }
  
}
