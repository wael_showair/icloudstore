//
//  iCloud+ZoneTests.swift
//  ExprimentalAppTests
//
//  Created by innovapost on 2017-09-25.
//  Copyright © 2017 Swifter. All rights reserved.
//

import XCTest
import CloudKit
import iCloudStore
import RxSwift

@testable import ExprimentalApp

class iCloud_ZoneTests: ICloudTestCase {
  
  func testFetchAllZones() {
    let expectation = XCTestExpectation(description: "iCloud Zone: fetch All is completed")
    self.iCloud.rx.fetchAllZones()
      .subscribe(
        onNext: { zones in
          XCTAssert(2 == zones.count)
          XCTAssert(zones.map{$0.zoneID.zoneName}.contains(self.iCloudZone.zoneID.zoneName))
          XCTAssert(zones.map{$0.zoneID.zoneName}.contains(CKRecordZoneDefaultName))
      },
        onError: { XCTAssert(false, "********** \(expectation) **********\n\($0)") },
        onCompleted: { expectation.fulfill() }
      )
      .disposed(by: bag)
    wait(for: [expectation], timeout: defaultTimeout)
  }
  
  func testZoneFetchCapability(){
    let expectation = XCTestExpectation(description: "iCloud Zone: fetch capability is completed")
    self.iCloudZone.rx.fetchCapabilities()
      .subscribe(
        onNext: { XCTAssert($0 == self.iCloudZone.capabilities) },
        onError: { XCTAssert(false, "********** \(expectation) **********\n\($0)") },
        onCompleted: { expectation.fulfill() }
      )
      .disposed(by: bag)
    wait(for: [expectation], timeout: defaultTimeout)
  }
}
